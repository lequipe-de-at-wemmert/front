import { connect } from 'react-redux'
import { LevelFilters, fetchUsers } from '../actions/userActions'
import { UserList } from '../components/users'

const getVisibleUsers = (users, filter) => {
  switch (filter) {
    case LevelFilters.SHOW_ALL:
      return users
    case LevelFilters.SHOW_NOVICE:
      return users.filter(u => u.isExpert)
    case LevelFilters.SHOW_EXPERTS:
      return users.filter(u => !u.isExpert)
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
  users: getVisibleUsers(state.users, state.userLevel)
})

const mapActionsToProps = {
    onFetchUsers: fetchUsers,
  }

export default connect(
  mapStateToProps,
  mapActionsToProps
)(UserList)