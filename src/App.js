import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import SignIn from './Auth/signin';
import SignUp from './Auth/singup';
import Dashboard from './Home/Dashboard'
import ChatBox from './components/chatbox'
import { UserList } from "./components/users";

const App  = () => {
  return (
      <>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/signIn">
            <SignIn />
          </Route>
          <Route path="/signUp">
            <SignUp />
          </Route>
          <Route path="/">
            <Dashboard />
            <ChatBox/>
          </Route>
        </Switch>
      </>
  );
}

export default App;
