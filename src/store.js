import reducers from './reducers'
import { messageMiddleware } from "./reducers/chatReducer";
import { createStore, compose, applyMiddleware } from "redux";

export const store = createStore(
    reducers,
    compose(
        applyMiddleware(messageMiddleware),
    )
  );