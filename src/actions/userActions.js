import { SET_USER_LEVEL, FETCH_USERS } from "./constants"

export const fetchUsers = (dispatch) => {
  return fetch('http://ndi.ep-operator.fr/api/users')
  .then(res => res.json())
  .then(response => {
    dispatch({
      type: FETCH_USERS,
      users: response
    });
  });
}

export const setLevelFilter = filter => ({
    type: SET_USER_LEVEL,
    filter
  })
  
  export const LevelFilters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_NOVICE: 'SHOW_NOVICE',
    SHOW_EXPERTS: 'SHOW_EXPERTS'
  }