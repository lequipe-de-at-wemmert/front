import { ON_MESSAGE } from "./constants";

export const sendMessage = (text, sender = "user") => ({
    type: ON_MESSAGE,
    text,
    sender
  });