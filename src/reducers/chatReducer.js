import { ApiAiClient } from "api-ai-javascript";
import { ON_MESSAGE } from "../actions/constants";
import { sendMessage } from "../actions/chatActions";

const accessToken = "c1259edad7dc4000b1571058a1746d2f";
const client = new ApiAiClient({ accessToken });

export const messageMiddleware = () => dispatch => action => {
  if (action.type === ON_MESSAGE) {
    const { text } = action;
    return client.textRequest(text).then(response => {
        const {
            result: { fulfillment }
          } = response;
          return dispatch(sendMessage(fulfillment.speech, "bot"));
    });
  }
};

export const messageReducer = (state = [], action) => {
  switch (action.type) {
    case ON_MESSAGE:
      return [action, ...state];

    default:
      return state;
  }
};
