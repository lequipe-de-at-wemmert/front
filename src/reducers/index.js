import { combineReducers } from 'redux';

import { messageReducer } from './chatReducer';
import userLevelFilter from './userLevelFilterReducer';
import userReducer from './userReducer';

export default combineReducers({
  chat: messageReducer,
  userLevel: userLevelFilter,
  users: userReducer,
});
