import { LevelFilters } from '../actions/userActions'
import { SET_USER_LEVEL } from '../actions/constants'

const userLevelFilter = (state = LevelFilters.SHOW_ALL, action) => {
  switch (action.type) {
    case SET_USER_LEVEL:
      return action.filter
    default:
      return state
  }
}

export default userLevelFilter