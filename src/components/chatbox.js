import React, { useEffect, useState } from "react";
import {
  Widget,
  addResponseMessage,
} from "react-chat-widget";
import "react-chat-widget/lib/styles.css";
import { connect } from "react-redux";
import { sendMessage } from "../actions/chatActions"

import logo from "../JeanCloud.svg";

const ChatBox = props => {

  useEffect(() => {
    addResponseMessage("Salut je suis Jean Cloud !");
  }, []);


  const { feed, sendMessage } = props;

  return (
    <>
      <Widget
        handleNewUserMessage={e => {
          sendMessage(e).then(e => {
            addResponseMessage(e.text);
          });
        }}
        title="Jean-Cloud"
        profileAvatar={logo}
        subtitle="Je suis la pour t'aider, pose moi une question."
        senderPlaceHolder="Ecris un message..."
        badge={feed.length}
      />
    </>
  );
};

const mapStateToProps = state => ({
  feed: state
});

export default connect(mapStateToProps, { sendMessage })(ChatBox);
