import door from "../door.png";
import sudoku from "../sudoku.jpg";
import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import NotificationsIcon from "@material-ui/icons/Notifications";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Hidden from "@material-ui/core/Hidden";
import Card from "@material-ui/core/Card";
import JeanCloud1 from "../JeanCloud/JeanCloud1.svg";
import JeanCloud2 from "../JeanCloud/JeanCloud2.svg";
import JeanCloud3 from "../JeanCloud/JeanCloud3.svg";
import JeanCloud4 from "../JeanCloud/JeanCloud4.svg";
import JeanCloud5 from "../JeanCloud/JeanCloud5.svg";
import JeanCloud6 from "../JeanCloud/JeanCloud6.svg";
import Button from "@material-ui/core/Button";
import gif from '../firework.gif'

const Secret = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const secondstep = urlParams.get("CeMessagePermetD'accederALaSecondeEtape");
  const [code, setCode] = useState("");
  const [resolved, setResolved] = useState(false);

  const submit = () => {
    if (code == "689024") {
      setResolved(true);
    }
  };

  const drawerWidth = 240;

  const useStyles = makeStyles(theme => ({
    root: {
      display: "flex"
    },
    toolbar: {
      paddingRight: 24 // keep right padding when drawer closed
    },
    toolbarIcon: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px",
      ...theme.mixins.toolbar
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginRight: 36
    },
    menuButtonHidden: {
      display: "none"
    },
    title: {
      flexGrow: 1
    },
    drawerPaper: {
      position: "relative",
      whiteSpace: "nowrap",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerPaperClose: {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9)
      }
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      height: "100vh",
      overflow: "auto"
    },
    container: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4)
    },
    paper: {
      padding: theme.spacing(2),
      display: "flex",
      overflow: "auto",
      flexDirection: "column"
    },
    fixedHeight: {
      height: 240
    },
    card: {
      maxWidth: 345,
      margin: 30
    },
    media: {
      height: 140
    }
  }));

  const classes = useStyles();

  return (
    <>
      {!resolved ? (
        <>
          <Container maxWidth="lg" className={classes.container}>
            <Grid alignItems="Center" direction="column" justify="center">
              <img src={door} />
              {secondstep != null ? (
                <img src={sudoku} />
              ) : (
                <p>
                  Je vous conseille, pour commencer, de demander à Jean-Cloud
                  s'il connaît le secret
                </p>
              )}
              <Grid justifyContent="center" justify="center">
                <ul>
                  <ul>
                    <input type="text" name="code" disabled value={code}/>
                  </ul>
                  <ul>
                    <Button onClick={() => setCode(code + "1")}>1</Button>
                    <Button onClick={() => setCode(code + "2")}>2</Button>
                    <Button onClick={() => setCode(code + "3")}>3</Button>
                  </ul>
                  <ul>
                    <Button onClick={() => setCode(code + "4")}>4</Button>
                    <Button onClick={() => setCode(code + "5")}>5</Button>
                    <Button onClick={() => setCode(code + "6")}>6</Button>
                  </ul>
                  <ul>
                    <Button onClick={() => setCode(code + "7")}>7</Button>
                    <Button onClick={() => setCode(code + "8")}>8</Button>
                    <Button onClick={() => setCode(code + "9")}>9</Button>
                  </ul>
                  <ul>
                    <Button
                      onClick={() => {
                        code.length > 0 &&
                          setCode(code.substr(0, code.length - 1));
                      }}
                    >
                      Del
                    </Button>
                    <Button onClick={() => setCode(code + "0")}>0</Button>
                    <Button onClick={submit}>Ok</Button>
                  </ul>
                </ul>
              </Grid>
            </Grid>
          </Container>
        </>
      ) : <img src={gif} alt="this slowpoke moves"  width="250"/>}
    </>
  );
};

export default Secret;
