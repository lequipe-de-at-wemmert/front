
import React, { useEffect, useState, useCallback } from "react";
import { LevelFilters, setLevelFilter } from "../actions/userActions";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";

export const UserList = ({users, onFetchUsers}) => {

    useEffect(() => onFetchUsers, []);

        return (
            <>
                <ChooseLevel/>
                {users.map(user => (
                    <User key={user.id} {...user} />
                ))}
            </>
        );

}

export const User = ({firstname, lastname, email}) => {
    return (
        <p>{firstname} {lastname} {email}</p>
    );
}

export const ChooseLevel = () => {
    const [ level, setLevel ] = useState(LevelFilters.SHOW_ALL);

    const handleChange = event => {
        setLevel(event.target.value);
        setLevelFilter(event.target.value);
    }

    return (
        <FormControl>
            <InputLabel id="user-level-label">Niveau d'expérience</InputLabel>
            <Select
            labelId="user-level-label"
            id="user-level"
            value={level}
            onChange={handleChange}
            >
            <MenuItem value={LevelFilters.SHOW_ALL}>Tout le monde</MenuItem>
            <MenuItem value={LevelFilters.SHOW_EXPERTS}>Experts</MenuItem>
            <MenuItem value={LevelFilters.SHOW_NOVICE}>Novices</MenuItem>
            </Select>
        </FormControl>  
    );
}
